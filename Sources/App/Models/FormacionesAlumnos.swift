//
//  File.swift
//  
//
//  Created by David Novella Giménez on 3/6/22.
//

import Vapor
import Fluent

typealias FormacionesAlumnos = [FormacionAlumno]

final class FormacionAlumno:Model, Content {
    static let schema = "formaciones+alumnos"

    @ID(key: .id) var id:UUID?
    @Parent(key: .alumnoID) var alumno:Alumno
    @Parent(key: .formacionID) var formacion:Formacion
    
    init() {}
    
    init(id:UUID? = nil, alumno:Int, formacion:UUID) {
        self.id = id
        self.$alumno.id = alumno
        self.$formacion.id = formacion
    }

}


struct CreateFormacionAlumno:Content {
    let id:UUID?
    let alumno:Int
    let formacion:UUID
}

struct UpdateFormacionAlumno:Content {
    let alumno:Int?
    let formacion:UUID?
}
