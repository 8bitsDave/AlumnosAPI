//
//  File.swift
//  
//
//  Created by David Novella Giménez on 4/6/22.
//

import Vapor
import Fluent

struct FormacionesAlumnosController:RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        let app = routes.grouped("api")

        app.group("formacionesAlumnos") { formaciones in
            formaciones.get(use: readFormacionesAlumnos)
            formaciones.post(use: createFormacionAlumno)
//            formaciones.group(":formacionID") { formacion in
//                formacion.get(use: alumnosEnFormacion)
//                empresa.patch(use: updateEmpresa)
//                empresa.delete(use: deleteEmpresa)
//                empresa.get("count", use: countAlumnos)
            }
        }
    
    //Esta función crea bien el registro pero no es capaz de encontrar duplicados
    func createFormacionAlumno(req:Request) async throws -> Int { //
        let content = try req.content.decode(CreateFormacionAlumno.self)
        guard let alumno = try await Alumno.find(content.alumno, on: req.db) else {
            throw Abort(.notFound, reason: "El alumno no existe") }
        guard let formacion = try await Formacion.find(content.formacion, on: req.db) else {
            throw Abort(.notFound, reason: "La formación no existe") }
        let alumnoID = try alumno.requireID()
        let formacionID = try formacion.requireID()
        //por lo que sea, pasa por aquí y no lanza el error
        let contenido = try await FormacionAlumno
            .query(on: req.db)
            .with(\.$alumno)
            .with(\.$formacion)
            .group(.and) { group in
                group
                    .filter(\.$alumno.$id == alumnoID)
                    .filter(\.$formacion.$id == formacionID)
            }
            .all()
        if contenido.count != 0 {
            Abort(.badRequest, reason: "El alumno \(alumno.nombre) ya está matriculado en la formación \(formacion.nombre)")
        }
        return contenido.count
    }
//            .count != 0  {
//                Abort(.badRequest, reason: "El alumno \(alumno.nombre) ya está matriculado en la formación \(formacion.nombre)")
//        }

        //        if try await ( FormacionAlumno
//            .query(on: req.db)
//            .with(\.$alumno)
//            .with(\.$formacion)
//            .group(.and) { group in
//                group
//                    .filter(\.$alumno.$id == content.alumno)
//                    .filter(\.$formacion.$id == content.formacion)
//            }
//            .all()
//            .count != 0  {
//                Abort(.badRequest, reason: "El alumno \(alumno.nombre) ya está matriculado en la formación \(formacion.nombre)")
//        }
        
//        let nuevaFormacionAlumno = FormacionAlumno(alumno: content.alumno, formacion: content.formacion)
//        try await nuevaFormacionAlumno.create(on: req.db)
//        return nuevaFormacionAlumno
//    }
//
    func readFormacionesAlumnos(req:Request) async throws -> FormacionesAlumnos {
        try await FormacionAlumno
            .query(on: req.db)
            .all()
    }
}
