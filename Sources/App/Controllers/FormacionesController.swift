//
//  File.swift
//  
//
//  Created by David Novella Giménez on 4/6/22.
//

import Vapor
import Fluent

struct FormacionesController:RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        let app = routes.grouped("api")
        app.group("formaciones") { formaciones in
            formaciones.get(use: readFormaciones)
            formaciones.post(use: createFormacion)
            formaciones.group(":formacionID") { formacion in
                formacion.get(use: readFormacion)
                formacion.patch(use: updateFormacion)
                formacion.delete(use: deleteFormacion)
            }
        }
    }
    func createFormacion(req:Request) async throws -> Formacion {
        let content = try req.content.decode(CreateFormacion.self)
    
        let nivelID = content.nivel
        guard let _ = try await Nivel.find(nivelID, on: req.db) else {
            throw Abort(.badRequest, reason: "El nivel introducido no existe")
        }
        if try await Formacion
            .query(on: req.db)
            .filter(\.$nombre, .custom("ILIKE"), content.nombre)
            .all()
            .count == 0 {
            let newFormacion = Formacion(nombre: content.nombre.capitalized,
                                         nivel: content.nivel,
                                         duracion: content.duracion,
                                         fechaInicio: content.fechaInicio)
            try await newFormacion.create(on: req.db)
            return newFormacion
        } else {
            throw Abort(.badRequest, reason: "Ya existe la formación: \(content.nombre)")
        }
    }

    func readFormaciones(req:Request) async throws -> Formaciones {
        try await Formacion
            .query(on: req.db)
            .with(\.$nivel)
            .all()
    }
    
    func readFormacion(req:Request) async throws -> Formacion {
        guard let id = req.parameters.get("formacionID", as: UUID.self) else { throw Abort(.badRequest, reason: "no sé qué pasa") }
        if let formacion = try await Formacion.find(id, on: req.db) {
            try await formacion.$nivel.load(on: req.db)
            return formacion
        } else {
            throw Abort(.notFound)
        }
    }
    
    func updateFormacion(req:Request) async throws -> Response {
        let content = try req.content.decode(UpdateFormacion.self)
        guard let id = req.parameters.get("formacionID", as: UUID.self) else { throw Abort(.badRequest) }
        guard let formacion = try await Formacion.find(id, on: req.db) else { throw Abort(.notFound) }
        try await formacion.$nivel.load(on: req.db)
        if let nombre = content.nombre, nombre != formacion.nombre {
            formacion.nombre = nombre
        }
        if let duracion = content.duracion, duracion != formacion.duracion {
            formacion.duracion = duracion
        }
        if let fechaInicio = content.fechaInicio, fechaInicio != formacion.fechaInicio {
            formacion.fechaInicio = fechaInicio
        }
        if let nivel = content.nivel, nivel != formacion.nivel.id {
            guard try await Nivel.find(nivel, on: req.db) != nil else {
                throw Abort(.notFound, reason: "Nivel no encontrado.")
            }
            formacion.nivel.id = nivel
        }
        try await formacion.update(on: req.db)
        return Response(status: .ok)
    }

    func deleteFormacion(req:Request) async throws -> Response {
        guard let id = req.parameters.get("formacionID", as: UUID.self) else { throw Abort(.badRequest) }
        if let formacion = try await Formacion.find(id, on: req.db) {
            try await formacion.delete(on: req.db)
            return Response(status: .ok)
        } else {
            throw Abort(.notFound)
        }
    }

}
